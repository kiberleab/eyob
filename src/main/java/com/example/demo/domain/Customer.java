package com.example.demo.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;
@Data
@Entity
@EqualsAndHashCode(exclude = "orders")
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "phone_no"))
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long Id;
	
	private String name;
	
	private String phone_no;
	
	private String password;
	
	@OneToMany(mappedBy="customer",cascade=CascadeType.ALL)
	private Set<AnOrder> anOrders;
	/**
	 * 
	 */
	public Customer() {
	}

	/**
	 * @param name
	 * @param phone_no
	 * @param password
	 */
	public Customer(String name, String phone_no, String password) {
		this.name = name;
		this.phone_no = phone_no;
		this.password = password;
	}

	/**
	 * @param id
	 * @param name
	 * @param phone_no
	 * @param password
	 */
	public Customer(Long id, String name, String phone_no, String password) {
		Id = id;
		this.name = name;
		this.phone_no = phone_no;
		this.password = password;
	}



	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone_no() {
		return phone_no;
	}

	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
