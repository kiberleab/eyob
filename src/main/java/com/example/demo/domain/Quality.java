package com.example.demo.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(exclude = "products")
public class Quality {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long Id;
	
	public boolean dye;
	

	public boolean preseretive;
	

	public boolean additive;

	public boolean fat;

	public boolean arifical_flavor;
	
	public String ph;

	@OneToMany(mappedBy = "quality", cascade = CascadeType.ALL)
    private Set<Product> products;
	/**
	 * @param id
	 * @param dye
	 * @param preseretive
	 * @param additive
	 * @param fat
	 * @param arifical_flavor
	 * @param ph
	 */
	public Quality(Long id, boolean dye, boolean preseretive, boolean additive, boolean fat, boolean arifical_flavor,
			String ph) {
		Id = id;
		this.dye = dye;
		this.preseretive = preseretive;
		this.additive = additive;
		this.fat = fat;
		this.arifical_flavor = arifical_flavor;
		this.ph = ph;
	}

	/**
	 * @param dye
	 * @param preseretive
	 * @param additive
	 * @param fat
	 * @param arifical_flavor
	 * @param ph
	 */
	public Quality(boolean dye, boolean preseretive, boolean additive, boolean fat, boolean arifical_flavor,
			String ph) {
		this.dye = dye;
		this.preseretive = preseretive;
		this.additive = additive;
		this.fat = fat;
		this.arifical_flavor = arifical_flavor;
		this.ph = ph;
	}

	/**
	 * 
	 */
	public Quality() {
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public boolean isDye() {
		return dye;
	}

	public void setDye(boolean dye) {
		this.dye = dye;
	}

	public boolean isPreseretive() {
		return preseretive;
	}

	public void setPreseretive(boolean preseretive) {
		this.preseretive = preseretive;
	}

	public boolean isAdditive() {
		return additive;
	}

	public void setAdditive(boolean additive) {
		this.additive = additive;
	}

	public boolean isFat() {
		return fat;
	}

	public void setFat(boolean fat) {
		this.fat = fat;
	}

	public boolean isArifical_flavor() {
		return arifical_flavor;
	}

	public void setArifical_flavor(boolean arifical_flavor) {
		this.arifical_flavor = arifical_flavor;
	}

	public String getPh() {
		return ph;
	}

	public void setPh(String ph) {
		this.ph = ph;
	}
	
	
	
	
}
