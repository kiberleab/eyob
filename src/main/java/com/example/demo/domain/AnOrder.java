package com.example.demo.domain;



import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@Entity
public class AnOrder{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long Id;
	
	private String timed;
	
	private int quantitiy;
	
	//Foreign Keys
	@ManyToOne
	@JoinColumn
	private Customer customer;
	@ManyToOne
	@JoinColumn
	private Product product;
	
	
}
