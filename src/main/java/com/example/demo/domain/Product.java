package com.example.demo.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.EqualsAndHashCode;
@Data
@Entity
@EqualsAndHashCode(exclude = "orders")
public class Product{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long Id;
	
	public String Name;

	public String Details;
	
	public int Price;
	
	public String catagory;
	
	public String Photo_url;
	
	@OneToMany(mappedBy="product",cascade=CascadeType.ALL)
	private Set<AnOrder> anOrders;
	
	@ManyToOne
    @JoinColumn
    private Quality quality;
	/**
	 * @param id
	 * @param name
	 * @param details
	 * @param price
	 * @param catagory
	 * @param photo_url
	 */
	public Product(Long id, String name, String details, int price, String catagory, String photo_url) {
		Id = id;
		Name = name;
		Details = details;
		Price = price;
		catagory = catagory;
		Photo_url = photo_url;
	}

	/**
	 * @param name
	 * @param details
	 * @param price
	 * @param catagory
	 * @param photo_url
	 */
	public Product(String name, String details, int price, String catagory, String photo_url) {
		Name = name;
		Details = details;
		Price = price;
		catagory = catagory;
		Photo_url = photo_url;
	}

	/**
	 * @param name
	 * @param details
	 * @param price
	 * @param catagory
	 */
	public Product(String name, String details, int price, String catagory) {
		Name = name;
		Details = details;
		Price = price;
		catagory = catagory;
	}

	/**
	 * 
	 */
	public Product() {
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getDetails() {
		return Details;
	}

	public void setDetails(String details) {
		Details = details;
	}

	public int getPrice() {
		return Price;
	}

	public void setPrice(int price) {
		Price = price;
	}

	public String getCatagory() {
		return catagory;
	}

	public void setCatagory(String catagory) {
		catagory = catagory;
	}

	public String getPhoto_url() {
		return Photo_url;
	}

	public void setPhoto_url(String photo_url) {
		Photo_url = photo_url;
	}
	
	
}
