package com.example.demo.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@Entity
public class Notice {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long Id;
	

	public String title;
	

	public String product;
	

	public String information;

	
	public String detail ;


	/**
	 * @param id
	 * @param title
	 * @param product
	 * @param information
	 * @param detail
	 */
	public Notice(Long id, String title, String product, String information, String detail) {
		Id = id;
		this.title = title;
		this.product = product;
		this.information = information;
		this.detail = detail;
	}


	/**
	 * @param title
	 * @param product
	 * @param information
	 * @param detail
	 */
	public Notice(String title, String product, String information, String detail) {
		this.title = title;
		this.product = product;
		this.information = information;
		this.detail = detail;
	}


	/**
	 * 
	 */
	public Notice() {
	}


	public Long getId() {
		return Id;
	}


	public void setId(Long id) {
		Id = id;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public String getInformation() {
		return information;
	}


	public void setInformation(String information) {
		this.information = information;
	}


	public String getDetail() {
		return detail;
	}


	public void setDetail(String detail) {
		this.detail = detail;
	}
	
	

	
}
