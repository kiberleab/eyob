package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.domain.AnOrder;

public interface OrderRepository extends CrudRepository<AnOrder,Long>{

}
