package com.example.demo.repository;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.domain.Product;;
public interface ProductRepository extends CrudRepository<Product,Long> {
//	List<> findByCompanyId(Long companyId);
	Iterable<Product> findAllByCatagory(String catagory);
}
