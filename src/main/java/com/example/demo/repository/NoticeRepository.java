package com.example.demo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.webmvc.RepositoryRestController;

import com.example.demo.domain.Notice;



@RepositoryRestController
public interface NoticeRepository extends CrudRepository<Notice,Long> {

}
