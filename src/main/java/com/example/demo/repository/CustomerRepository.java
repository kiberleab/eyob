package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.webmvc.RepositoryRestController;

import com.example.demo.domain.Customer;

@RepositoryRestController
public interface CustomerRepository extends  CrudRepository<Customer,Long> {

}
