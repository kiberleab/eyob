package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Customer;

import com.example.demo.repository.CustomerRepository;

@RestController
public class CustomerController {
	@Autowired
	private CustomerRepository customerRepository;
	@ResponseBody
	@RequestMapping(path="/customer",produces="application/hal+json",method=RequestMethod.POST)
	public Customer addCustomer(@RequestBody Customer customer) {
		return this.customerRepository.save(customer);
	}
	@ResponseBody
	@RequestMapping(path="/customer",produces="application/hal+json",method=RequestMethod.GET)
	public Customer getCustomer(@RequestParam("id") long id) {
		return this.customerRepository.findById(id).get();
	}
	@ResponseBody
	@RequestMapping(path="/customer/all",produces="application/hal+json",method=RequestMethod.GET)
	public Iterable<Customer> getAllCustomer() {
		return this.customerRepository.findAll();
	}
}
