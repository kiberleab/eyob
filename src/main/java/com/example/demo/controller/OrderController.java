package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.AnOrder;
import com.example.demo.repository.OrderRepository;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@RestController
public class OrderController {
	@Autowired
	private OrderRepository orderRepository;;
	
	///THis To be Modifed
	@ResponseBody
	@RequestMapping(path="order/customer",produces="application/hal+json",method=RequestMethod.GET)
	public Iterable<AnOrder> getAllOrdersByCustomerId(@RequestParam(value="customerId") Long customerId) {
			//TODO()
		return this.orderRepository.findAll();
	}
	@ResponseBody
	@RequestMapping(path="order/all",produces="application/hal+json",method=RequestMethod.GET)
	public Iterable<AnOrder> getAllOrders() {
			//TODO()
		return this.orderRepository.findAll();
	}
	@ResponseBody
	@RequestMapping(path="order",produces="application/hal+json",method=RequestMethod.POST)
	public AnOrder inserOrder(@RequestBody AnOrder anOrder) {
			return this.orderRepository.save(anOrder);
		}
	@ResponseBody
	@RequestMapping(path="order",produces="application/hal+json",method=RequestMethod.GET,consumes = "application/x-www-form-urlencoded")
	public AnOrder insertCha(@RequestParam(value="id") long id) {
			return this.orderRepository.findById(id).get();
		}
	@ResponseBody
	@RequestMapping(path="order",produces="application/hal+json",method=RequestMethod.DELETE)
	public void deleteOrder(@RequestParam(value="id") long id) {
			 this.orderRepository.deleteById(id);
		}
	
	
}
