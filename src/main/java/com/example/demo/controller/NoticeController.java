package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Notice;
import com.example.demo.repository.NoticeRepository;

@RestController
public class NoticeController {
	
	@Autowired
	private NoticeRepository noticeRepository;
	@ResponseBody
	@RequestMapping(path="notice",produces="application/hal+json",method=RequestMethod.GET)
	public Iterable<Notice> getAppointmentsByUser() {
			return this.noticeRepository.findAll();
		}
	@ResponseBody
	@RequestMapping(path="api/Services",produces="application/hal+json",method=RequestMethod.POST)
	public Notice insertService(@RequestBody Notice notice) {
			return this.noticeRepository.save(notice);
		}
	@ResponseBody
	@RequestMapping(path="api/Services",produces="application/hal+json",method=RequestMethod.DELETE)
	public void deleteService(@RequestParam(value="id") Long id) {
			this.noticeRepository.deleteById(id);
		}
	
}
