package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Product;
import com.example.demo.repository.ProductRepository;

@RestController
public class ProductController {
	@Autowired
	private ProductRepository productRepository;
	
	@ResponseBody
	@RequestMapping(path="product",produces="application/hal+json",method=RequestMethod.POST)
	public Product insertproduct(@RequestBody Product Product) {
			return this.productRepository.save(Product);
		}
	@ResponseBody
	@RequestMapping(path="product/catagory",produces="application/hal+json",method=RequestMethod.GET)
	public Iterable<Product> getProductsByCatagory(@RequestParam("catagory") String  catagory) {
			return this.productRepository.findAllByCatagory(catagory);
		}

	@ResponseBody
	@RequestMapping(path="product",produces="application/hal+json",method=RequestMethod.GET)
	public Product getProductsById(@RequestParam("id") long  id) {
			return this.productRepository.findById(id).get();
		}
	@ResponseBody
	@RequestMapping(path="product",produces="application/hal+json",method=RequestMethod.DELETE)
	public void deleteProductsById(@RequestParam("id") long  id) {
			this.productRepository.deleteById(id);
		}
}
